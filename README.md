# Entity Component System for Javascript

[![Pipeline status](https://gitlab.com/ecsjs/ecs/badges/master/pipeline.svg)](https://gitlab.com/ecsjs/ecs/-/pipelines)
[![NPM version](https://img.shields.io/npm/v/ecsjs.svg)](https://www.npmjs.org/package/ecsjs)
[![NPM downloads](https://img.shields.io/npm/dm/ecsjs.svg)](https://npmjs.org/package/ecsjs "View this project on NPM")

[![BuyMeACoffee](https://www.buymeacoffee.com/assets/img/custom_images/purple_img.png)](https://www.buymeacoffee.com/peterf)

An entity component system library for JavaScript

## Install

### NPM
- Install: `npm install --save ecsjs`

## Usage

[Examples](http://ecsjs.gitlab.io/ecs-examples/)

### Browser Module

```js
import {ecs} from './some-path/ecs.js'

class PositionComponent {
  constructor(x, y) {
    this.x = x
    this.y = y
  }
}

// register a component
ecs.register(PositionComponent)

// create an entity
const entityId = ecs.getNextId()
ecs.set(entityId, new PositionComponent(25, 25))
```

### Browser

```html
<script type="application/javascript" src="./some-path/ecs.js"></script>
<script>
  class PositionComponent {
    constructor(x, y) {
      this.x = x
      this.y = y
    }
  }

  // register a component
  ecs.register(PositionComponent)

  // create an entity
  const entityId = ecs.getNextId()
  ecs.set(entityId, new PositionComponent(25, 25))
</script>
```

## License

Licensed under GNU GPL v3

Copyright &copy; 2013+ [contributors](https://gitlab.com/ecsjs/ecs/-/graphs/master)