import esbuild from 'esbuild';
import { resolve } from 'node:path';

const projectPath = process.cwd();
const sourcePath = resolve(projectPath, 'src');
const distPath = resolve(projectPath, 'dist');
const isDevEnv = process.env?.BUNDLE_DEV;
const sourceEntryPoint = resolve(sourcePath, 'ecs.js');
const outputFile = 'ecs.js';
const minify = !isDevEnv;

await esbuild.build({
  entryPoints: [sourceEntryPoint],
  outfile: resolve(distPath, outputFile),
  platform: 'node',
  format: 'esm',
  mainFields: ['module'],
  sourcemap: 'linked',
  bundle: true,
  minify
});