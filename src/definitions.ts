import type { KeyMap } from './key-map.js'

export type KeyCollection<T> = { [key: string]: T }

export interface IComponent { }

export type ComponentClass<T extends IComponent> = {
  new(...args: any[]): T
}

export type ComponentDataMap<T> = KeyMap<number, T>

export type ComponentMap = KeyMap<string, ComponentDataMap<any>>