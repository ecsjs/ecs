/*
 ecsjs is an entity component system library for JavaScript
 Copyright (C) 2014 Peter Flannery

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @module ecsjs
 */
import {
  type ComponentDataMap,
  type ComponentClass,
  type ComponentMap,
  type IComponent
} from './definitions.js';
import { KeyMap } from './key-map.js'

/**
 * Class for storing entities and their relationships
 * 
 * @class EntityMap
 */
export class EntityMap {

  // entity class maps
  private componentMaps: ComponentMap = new KeyMap()

  private nextId: number = 0

  constructor() { }

  /**
   * Registers a component class
   * 
   * @example
   *
   * ```javascript
   *    // component class
   *    class MyComponent {
   *      constructor(x) {
   *        this.x = x;
   *      }
   *    }
   *
   *    ecs.register(MyComponent);
   *    // or
   *    ecs.register(MyComponent1, MyComponent2);
   * ```
   */
  register(...component: ComponentClass<any>[]) {
    for (const definition of component) {
      const componentName = definition.name;
      if (componentName === undefined)
        throw new ReferenceError("The component class does not have a name defined. i.e. a constructor name");

      // create the component map
      const componentDataMap: ComponentDataMap<any> = new KeyMap();
      this.componentMaps.set(componentName, componentDataMap);
    }

    // chain
    return this;
  }

  /**
   * Retrieves an entity map by it's registered type
   */
  getMap<T extends IComponent>(component: ComponentClass<T>): ComponentDataMap<T> | undefined {
    return this.componentMaps.get(component.name);
  }

  /**
   * Get the first entity entry for a component class
   * 
   * @example
   *
   * ```javascript
   *    const [entityId, player] = ecs.firstEntry(PlayerComponent) ?? [];
   * ```
   */
  firstEntry<T extends IComponent>(component: ComponentClass<T>) {
    return this.getMap(component)?.firstEntry();
  }

  /**
   * Get the first entity id for a component class
   * 
   * @example
   *
   * ```javascript
   *    const entityId = ecs.firstKey(PlayerComponent);
   * ```
   */
  firstKey<T extends IComponent>(component: ComponentClass<T>) {
    return this.getMap(component)?.firstKey();
  }

  /**
   * Get the first entity component value for a component class
   * 
   * @example
   *
   * ```javascript
   *    const player = ecs.firstValue(PlayerComponent);
   * ```
   */
  firstValue<T extends IComponent>(component: ComponentClass<T>) {
    return this.getMap(component)?.firstValue();
  }

  /**
   * Retrieves an entity from its registered entity map
   *
   * @example
   *
   * ```javascript
   *   // get an entity
   *   let positionData = ecs.get(entityId, Components.PositionComponent);
   * ```
   */
  get<T extends IComponent>(entityId: number, component: ComponentClass<T>): T {
    let cm = this.componentMaps.get(component.name);
    if (cm === undefined)
      throw new ReferenceError(`"${component}" component class does not exist in the componentMaps`);

    return cm.get(entityId);
  }

  /**
   * Retrieves an entity from its registered entity map
   *
   * @example
   *
   * ```javascript
   *   // get an entity
   *   let positionData = ecs.getByKey(entityId, "PositionComponent");
   * ```
   */
  getByKey(entityId: number, componentName: string) {
    // get the component map
    let map = this.componentMaps.get(componentName);
    if (map === undefined)
      throw new ReferenceError(`"${componentName}" component does not exist in the componentMaps`);

    return map.get(entityId);
  }

  /**
   * Checks if the entity exists in it's registed component map
   * The entity type must be registered before calling this method
   * 
   * @example
   *
   * ```javascript
   *   // get an entity
   *   let exists = ecs.has(entityId, PositionComponent);
   * ```
   */
  has<T extends IComponent>(entityId: number, component: ComponentClass<T>): boolean {
    // get the component map
    let map = this.componentMaps.get(component.name);
    if (map === undefined) return false

    return map.has(entityId);
  }

  /**
   * Adds or updates an entity to it's registered entity map
   *
   * @example
   *
   * ```javascript
   *   // add/update an entity
   *   let playerPos = ecs.set(entityId, new Components.PositionComponent(0, 0));
   * ```
   */
  set<T extends IComponent>(entityId: number, componentData: T): T {
    // get the component map
    let map = this.componentMaps.get(componentData.constructor.name);
    if (map === undefined)
      throw new ReferenceError(`Component map does not exist using the name: ${componentData.constructor.name}`);

    // set the entity on the entity map
    map.set(entityId, componentData);

    // return instance
    return componentData as T;
  }

  /**
   * Removes an entity from its entity map.
   * Removes any related Node entities that are mapped to the entity
   * 
   * @example
   *
   * ```javascript
   *   // remove an entity from its entity map
   *   EntityMap.remove(entityId, Components.PositionComponent);
   * ```
   */
  remove<T extends IComponent>(entityId: number, component: ComponentClass<T>) {
    return this.removeByKey(entityId, component.name);
  }

  /**
   * Removes an entity from its entity map.
   * Removes any related Node entities that are mapped to the entity
   * 
   * @example
   *
   * ```javascript
   *   // remove an entity from its entity map
   *   EntityMap.removeByKey(entityId, "PositionComponent");
   * ```
   */
  removeByKey(entityId: number, componentName: string) {
    // get the entity map
    let entityMap = this.componentMaps.get(componentName);

    // ensure the map is defined
    if (entityMap === undefined)
      throw new ReferenceError(`A registered entity map does not exist for the given key: ${componentName}`);

    // get the entity
    let entity = entityMap.get(entityId);
    if (entity === undefined) return false;

    // remove the entity from the entity map
    return entityMap.delete(entityId);
  }

  /**
   * Destroys an entity across all component maps
   */
  destroyEntity(entityId: number) {
    this.componentMaps.forEach((dataMap: ComponentDataMap<any>) => {
      if (dataMap.has(entityId)) dataMap.delete(entityId);
    });
  }

  // TODO generate a non repeatable id for network play?
  getNextId() {
    this.nextId++;
    return this.nextId;
  }

  clear() {
    this.componentMaps.clear();
    return this;
  }

  /**
   * Prints all entity maps in a tabular format to the console
   */
  printTable() {
    this.componentMaps.forEach(map => console.table(map.toTable(true)));
    return this
  }

  /**
   * Parse's the JSON and returns an EntityMap object
   * 
   * @example
   *
   * ```javascript
   *   // remove an entity from its entity map
   *   const json = JSON.stringfy(ecs);
   *   const restoredMap = EntityMap.parse(json);
   * ```
   */
  static parse(json: string) {
    const ecs = new EntityMap();

    const restored = JSON.parse(json, function (key: string, value: any) {

      if (value.hasOwnProperty('componentMaps')) {
        Reflect.setPrototypeOf(value, EntityMap.prototype);
        return value;
      }

      if (value.hasOwnProperty('__KeyMap__')) {
        return new KeyMap(value.iterable);
      }

      return this[key];
    });

    ecs.componentMaps = restored.componentMaps;
    ecs.nextId = restored.nextId;
    return ecs;
  }

  /**
   * A tracing method used for debugging.
   * Intercepts all functions specified and logs each call to the console.
   * 
   * @method createWithTracing
   * @static
   * @param {Array} funcFilter A list of function names you want to intercept. If no function names are specified then will log all functions called
   * @return {EntityMap} A new entity map with tracing enabled
   */
  static createWithTracing(funcFilter: any) {
    let traceHandler = {
      get(target: any, propKey: string) {
        const targetValue = target[propKey]

        if (typeof targetValue === 'function' && (funcFilter.length === 0 || funcFilter.includes(propKey))) {
          return function (this: any, ...args: any[]) {
            console.groupCollapsed('ecs trace', propKey, args);
            console.trace();
            console.groupEnd();
            return targetValue.apply(this, args);
          }
        }

        return targetValue;
      }
    }

    return new Proxy(new EntityMap(), traceHandler)
  }

}