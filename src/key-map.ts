/*
 ecsjs is an entity component system library for JavaScript
 Copyright (C) 2014 Peter Flannery

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import type { KeyCollection } from './definitions.js';

/**
 * Class for storing keys and values
 */
export class KeyMap<TKey, TValue> extends Map<TKey, TValue> {

  constructor(entries: any[] = []) {
    super(entries)
  }

  firstEntry(): [key: TKey, value: TValue] | undefined {
    if (this.size === 0) return undefined;

    const iterator = this.entries();
    const result = iterator.next();
    return result.value;
  }

  firstKey(): TKey | undefined {
    if (this.size === 0) return undefined;

    const iterator = this.keys();
    const result = iterator.next();
    return result.value;
  }

  firstValue(): TValue | undefined {
    if (this.size === 0) return undefined;

    const iterator = this.values();
    const result = iterator.next();
    return result.value;
  }

  toJSON() {
    return { __KeyMap__: 1, iterable: [...this.entries()] }
  }

  toTable(excludeKeys = false): any[] {
    const table = []
    for (const value of this.entries()) {
      let entity = value[1] as { new(): TValue }
      let meta: KeyCollection<any> = {}

      if (excludeKeys == false) meta["entity.key"] = value[0]
      meta["entity.type"] = entity.constructor.name

      table.push({ ...meta, ...entity })
    }
    return table
  }

  /**
   * Outputs keys and values in a tabular format to the console
   */
  printTable(excludeKeys = false): void {
    console.table(this.toTable(excludeKeys))
  }

  entries(): MapIterator<any> {
    return super.entries().filter(x => x !== undefined && x !== null)
  }

  keys(): MapIterator<any> {
    return super.keys().filter(x => x !== undefined && x !== null)
  }

  values(): MapIterator<any> {
    return super.values().filter(x => x !== undefined && x !== null)
  }

  // default iterator
  [Symbol.iterator]() {
    return this.values()
  }

  static get [Symbol.species]() { return Map; }

}