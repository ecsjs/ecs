import { type KeyCollection } from '#ecs/definitions.js'

export default {

  Component1: class {
    constructor(readonly x = 0, readonly y = 0, readonly z = 0) {
      this.x = x
      this.y = y
      this.z = z
    }
  },

  Component2: class {
    constructor(readonly a = 0, readonly b = 0, readonly c = 0) {
      this.a = a
      this.b = b
      this.c = c
    }
  },

  Component3: class {
    sides: number
    rotation: number
    radius: number
    fill: KeyCollection<any> = {}  //
    stroke: KeyCollection<any> = {}

    constructor() {
      this.sides = 3
      this.rotation = 0
      this.radius = 4
      this.fill = {}  // rgba type
      this.stroke = {}  // rgba type
    }
  }
}
