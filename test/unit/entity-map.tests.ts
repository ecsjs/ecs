import { EntityMap } from "#ecs/entity-map.js"
import assert from 'node:assert'
import ComponentFixtures from "./component.fixtures.js"
// @ts-ignore
import { expect } from "mochaccino"
import { KeyMap } from '#ecs/key-map.js'

function createTestEntityMap() {
  let em = new EntityMap()

  em.register(ComponentFixtures.Component1)
  em.register(ComponentFixtures.Component2)
  em.register(ComponentFixtures.Component3)

  let components = [
    new ComponentFixtures.Component1(1, 2, 3),
    new ComponentFixtures.Component2(7, 8, 9),
    new ComponentFixtures.Component3()
  ];

  // create some entities with Component1 models
  const entityId = em.getNextId()
  components.forEach(em.set.bind(em, entityId))

  return em
}

let ecs: EntityMap

export const ComponentTests = {

  beforeEach: function () {
    ecs = new EntityMap()
  },

  register: {

    "Can register single components": () => {
      // register the component id's
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)
      ecs.getMap(ComponentFixtures.Component1)
      // assert
      expect(ecs.getMap(ComponentFixtures.Component1)).not.toBeUndefined()
      expect(ecs.getMap(ComponentFixtures.Component2)).not.toBeUndefined()
      expect(ecs.getMap(ComponentFixtures.Component3)).not.toBeUndefined()
    },

    "Can register components from an array": () => {
      // register the component id's
      ecs.register(...[
        ComponentFixtures.Component1,
        ComponentFixtures.Component2,
        ComponentFixtures.Component3
      ])

      // assert
      expect(ecs.getMap(ComponentFixtures.Component1)).not.toBeUndefined()
      expect(ecs.getMap(ComponentFixtures.Component2)).not.toBeUndefined()
      expect(ecs.getMap(ComponentFixtures.Component3)).not.toBeUndefined()
    },

    "Can register components as a param array": () => {
      // register the component id's
      ecs.register(
        ComponentFixtures.Component1,
        ComponentFixtures.Component2,
        ComponentFixtures.Component3
      )

      // assert
      expect(ecs.getMap(ComponentFixtures.Component1)).not.toBeUndefined()
      expect(ecs.getMap(ComponentFixtures.Component2)).not.toBeUndefined()
      expect(ecs.getMap(ComponentFixtures.Component3)).not.toBeUndefined()
    }

  },

  getNextId: {

    "generates unique key": () => {
      // create the entities
      let entityId1 = ecs.getNextId();
      let entityId2 = ecs.getNextId();
      let entityId3 = ecs.getNextId();

      expect(entityId1).not.toBeUndefined();
      expect(entityId2).not.toBeUndefined();
      expect(entityId3).not.toBeUndefined();

      expect(entityId1).not.toBe(entityId2);
      expect(entityId1).not.toBe(entityId3);

      expect(entityId2).not.toBe(entityId1);
      expect(entityId2).not.toBe(entityId3);

      expect(entityId3).not.toBe(entityId1);
      expect(entityId3).not.toBe(entityId2);
    }

  },

  firstEntry: {
    "returns the first entry": () => {
      // setup
      ecs.register(ComponentFixtures.Component1)
      const entityId = ecs.getNextId()
      const component = ecs.set(entityId, new ComponentFixtures.Component1())
      // test
      let [key, value] = ecs.firstEntry(ComponentFixtures.Component1) ?? []
      // assert
      expect(key).toBe(entityId)
      expect(value).toEqual(component)
    },
    "returns undefined when a map is empty": () => {
      // setup
      ecs.register(ComponentFixtures.Component1)
      // test
      let [key, value] = ecs.firstEntry(ComponentFixtures.Component1) ?? []
      // assert
      expect(key).toBe(undefined)
      expect(value).toEqual(undefined)
    }
  },

  firstKey: {
    "returns the first key": () => {
      // setup
      ecs.register(ComponentFixtures.Component1)
      const entityId = ecs.getNextId()
      ecs.set(entityId, new ComponentFixtures.Component1())
      // test
      const key = ecs.firstKey(ComponentFixtures.Component1)
      // assert
      expect(key).toBe(entityId)
    },
    "returns undefined when a map is empty": () => {
      // setup
      ecs.register(ComponentFixtures.Component1)
      // test
      const key = ecs.firstKey(ComponentFixtures.Component1)
      // assert
      expect(key).toBe(undefined)
    }
  },

  firstValue: {
    "returns the first value": () => {
      // setup
      ecs.register(ComponentFixtures.Component1)
      const entityId = ecs.getNextId()
      const component = ecs.set(entityId, new ComponentFixtures.Component1())
      // test
      const value = ecs.firstValue(ComponentFixtures.Component1)
      // assert
      expect(value).toBe(component)
    },
    "returns undefined when a map is empty": () => {
      // setup
      ecs.register(ComponentFixtures.Component1)
      // test
      const value = ecs.firstKey(ComponentFixtures.Component1)
      // assert
      expect(value).toBe(undefined)
    }
  },

  get: {

    "returns an existing entity": () => {
      // register the component id's
      ecs.register(ComponentFixtures.Component1);

      // create the entity id
      const entityId = ecs.getNextId()

      // add the component to the entity
      const componentModel1 = ecs.set(entityId, new ComponentFixtures.Component1());

      // get the component
      let testComponent = ecs.get(entityId, ComponentFixtures.Component1);

      // check the component is the same instance
      expect(testComponent).toBe(componentModel1);
    },

    "returns undefined for non-existing entity": () => {
      // register the component id's
      ecs.register(ComponentFixtures.Component1);
      ecs.register(ComponentFixtures.Component2);

      // create our test entity
      let testEntityId = ecs.getNextId();

      // add the component to the entity
      ecs.set(testEntityId, new ComponentFixtures.Component1());

      // attempt to get non-existing component
      let testComponent = ecs.get(testEntityId, ComponentFixtures.Component2);

      // check the component is the same instance
      expect(testComponent).toBeUndefined();
    }

  },

  set: {

    "creates access relation via another related components": () => {
      // register the entities
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)

      // create an entity and attach related entities
      const entityId = ecs.getNextId()
      const expected1 = ecs.set(entityId, new ComponentFixtures.Component1())
      const expected2 = ecs.set(entityId, new ComponentFixtures.Component2())

      // test the component instances are stored on the entity
      let testComponent1 = ecs.get(entityId, ComponentFixtures.Component1)
      if (testComponent1 === undefined)
        assert.fail()
      else
        expect(testComponent1).toBe(expected1)

      let testComponent2 = ecs.get(entityId, ComponentFixtures.Component2)
      if (testComponent2 === undefined)
        assert.fail()
      else
        expect(testComponent2).toBe(expected2)
    },

    "throws an error when an entity map doesnt exist.": () => {
      // register a component
      ecs.register(ComponentFixtures.Component1);

      // create our test entity
      const entityId = ecs.getNextId();

      // create a component that doesnt exist
      const componentModel2 = new ComponentFixtures.Component2();

      // assert
      expect(() => ecs.set(entityId, componentModel2))
        .toThrow(new ReferenceError("Entity map does not exist."))
    }

  },

  has: {

    "returns true when component is related": () => {
      // register the entities
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)

      // create an entity and attach related entities
      const entityId = ecs.getNextId()
      ecs.set(entityId, new ComponentFixtures.Component1())
      ecs.set(entityId, new ComponentFixtures.Component2())
      ecs.set(entityId, new ComponentFixtures.Component3())

      // assert
      expect(ecs.has(entityId, ComponentFixtures.Component2)).toBe(true)
      expect(ecs.has(entityId, ComponentFixtures.Component3)).toBe(true)
    },

    "returns false when component is not related": () => {
      // register the entities
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)

      // create an entity and attach related entities
      const entityId = ecs.getNextId()
      ecs.set(entityId, new ComponentFixtures.Component1())

      // assert
      expect(ecs.has(entityId, ComponentFixtures.Component2)).toBe(false)
      expect(ecs.has(entityId, ComponentFixtures.Component3)).toBe(false)
    }

  },

  remove: {

    "removes related components": () => {
      // register the entities
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)

      // create an entity and attach related entities
      const entityId = ecs.getNextId()
      ecs.set(entityId, new ComponentFixtures.Component1())
      ecs.set(entityId, new ComponentFixtures.Component2())
      ecs.set(entityId, new ComponentFixtures.Component3())

      // assert
      expect(ecs.has(entityId, ComponentFixtures.Component2)).toBe(true)
      ecs.remove(entityId, ComponentFixtures.Component2)
      expect(ecs.has(entityId, ComponentFixtures.Component2)).toBe(false)
    },

    "removes specified component list": () => {
      // register the entities
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)

      // create an entity and attach related entities
      const entityId = ecs.getNextId()
      ecs.set(entityId, new ComponentFixtures.Component1())
      ecs.set(entityId, new ComponentFixtures.Component2())
      ecs.set(entityId, new ComponentFixtures.Component3())

      // assert
      ecs.remove(entityId, ComponentFixtures.Component2)
      ecs.remove(entityId, ComponentFixtures.Component3)

      expect(ecs.has(entityId, ComponentFixtures.Component1)).toBe(true)
      expect(ecs.has(entityId, ComponentFixtures.Component2)).toBe(false)
      expect(ecs.has(entityId, ComponentFixtures.Component3)).toBe(false)
    }

  },

  destroyEntity: {

    "destroys all components related to entity": () => {
      // register the entities
      ecs.register(ComponentFixtures.Component1)
      ecs.register(ComponentFixtures.Component2)
      ecs.register(ComponentFixtures.Component3)

      // create an entity and attach related entities
      const entityId = ecs.getNextId()
      ecs.set(entityId, new ComponentFixtures.Component1())
      ecs.set(entityId, new ComponentFixtures.Component2())
      ecs.set(entityId, new ComponentFixtures.Component3())

      // assert
      ecs.destroyEntity(entityId)

      expect(ecs.has(entityId, ComponentFixtures.Component1)).toBe(false)
      expect(ecs.has(entityId, ComponentFixtures.Component2)).toBe(false)
      expect(ecs.has(entityId, ComponentFixtures.Component3)).toBe(false)
    }

  },

  parse: {

    "restores ecs from JSON": () => {
      ecs = createTestEntityMap()

      const serialized = JSON.stringify(ecs)
      const restored = EntityMap.parse(serialized)

      expect(restored).toBeDefined()
      expect(Reflect.getPrototypeOf(restored)).toBe(EntityMap.prototype)

      expect(restored['componentMaps']).toBeDefined()
      expect(Reflect.getPrototypeOf(restored['componentMaps'])).toBe(KeyMap.prototype)
      expect(restored['componentMaps'].size).toBe(ecs['componentMaps'].size)
    },

  }

}