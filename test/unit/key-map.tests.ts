import { KeyMap } from "#ecs/key-map.js";
// @ts-ignore
import { expect } from "mochaccino"

function getIterationTestData() {
  let data = [];
  for (let index = 0; index < 5; index++) {
    data.push({
      key: index,
      value: String.fromCharCode(65 + index)
    });
  }
  return data
}

export const KeyMapTests = {

  get: {

    "returns existing items": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");

      // assert
      expect(map.get(1)).toBe("testing 1");
      expect(map.get(2)).toBe("testing 2");
      expect(map.get(3)).toBe("testing 3");
    },

    "returns undefined for non existing items": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");

      // assert
      expect(map.get(4)).toBeUndefined();
      expect(map.get(5)).toBeUndefined();
      expect(map.get(6)).toBeUndefined();
    }

  },

  set: {

    "overwrites existing items": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");

      map.set(1, "testing 4");
      map.set(2, "testing 5");
      map.set(3, "testing 6");

      // assert
      expect(map.get(1)).toBe("testing 4");
      expect(map.get(2)).toBe("testing 5");
      expect(map.get(3)).toBe("testing 6");
    }

  },

  delete: {

    "removes and returns true for existing keys": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");

      let result = map.delete(2);

      // assert
      expect(map.get(2)).toBeUndefined();
      expect(map.has(2)).toBeFalsy();
      expect(result).toBeTruthy();
    },

    "returns false for non-existing keys": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");

      let result = map.delete(5);

      // assert
      expect(result).toBeFalsy();
    }

  },

  has: {

    "returns true for existing keys": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");

      // assert
      expect(map.has(2)).toBeTruthy();
    },

    "returns false for non-existing keys": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(5, "testing 2");
      map.set(22, "testing 3");

      // assert
      expect(map.has(2)).toBeFalsy();
    }

  },

  firstEntry: {

    "returns the first entry": () => {
      // setup
      let map = new KeyMap<number, string>();
      map.set(112, "testing 1");
      map.set(5, "testing 2");
      map.set(22, "testing 3");
      // get the entities iterator
      let [key, value] = map.firstEntry() ?? [];
      // assert
      expect(key).toBe(112);
      expect(value).toBe("testing 1");
    },

    "returns undefined when a map is empty": () => {
      // setup
      let map = new KeyMap<number, string>();
      // test
      let firstEntry = map.firstEntry();
      // assert
      expect(firstEntry).toEqual(undefined);
    }

  },

  firstKey: {

    "returns the first key": () => {
      // setup
      let map = new KeyMap<number, string>();
      map.set(112, "testing 1");
      map.set(5, "testing 2");
      map.set(22, "testing 3");
      // get the entities iterator
      let actualKey = map.firstKey();
      // assert
      expect(actualKey).toBe(112);
    },

    "returns undefined when a map is empty": () => {
      // setup
      let map = new KeyMap<number, string>();
      // test
      let actualKey = map.firstKey();
      // assert
      expect(actualKey).toEqual(undefined);
    }

  },

  firstValue: {

    "returns the first value": () => {
      // setup
      let map = new KeyMap<number, string>();
      map.set(112, "testing 1");
      map.set(5, "testing 2");
      map.set(22, "testing 3");
      // get the entities iterator
      let actualValue = map.firstValue();
      // assert
      expect(actualValue).toBe("testing 1");
    },

    "returns undefined when a map is empty": () => {
      // setup
      let map = new KeyMap<number, string>();
      // test
      let actualValue = map.firstKey();
      // assert
      expect(actualValue).toEqual(undefined);
    }

  },

  forEach: {

    "loops all items and keys": () => {
      // setup
      let expectedData = getIterationTestData();

      let testMap = new KeyMap();
      expectedData.forEach((expected) => {
        testMap.set(expected.key, expected.value);
      });

      // test, assert
      let counter = 0;
      testMap.forEach((value, key) => {
        expect(value).toBe(expectedData[counter].value);
        expect(key).toBe(expectedData[counter].key);
        counter++;
      });

      expect(counter).toBe(5);
    },

    "loop skips undefined or null keys": () => {
      // setup
      const testData = [
        [undefined, undefined],
        [2, {}],
        [null, null],
        [4, {}],
        [5, {}]
      ];

      let testMap = new KeyMap(testData);

      // test, assert
      let counter = 0;
      for (const element of testMap) {
        expect(element).toBeDefined();
        expect(element).not.toBeNull();
        counter++;
      }

      expect(counter).not.toBe(0);
      expect(testMap.size).not.toBe(0)
    }

  },

  keys: {

    "iterates all keys": () => {

      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");
      map.set(4, "testing 4");
      map.set(5, "testing 5");

      let counter = 1;
      let iter = map.keys()

      for (let key of iter) {
        expect(key).toBe(counter);
        counter++;
      }

      expect(counter).toBe(6);
    },

    "iteration skips undefined or null keys": () => {
      // setup
      const testData = [
        [undefined, undefined],
        [2, {}],
        [null, null],
        [4, {}],
        [5, {}]
      ];

      let testMap = new KeyMap(testData);

      // test, assert
      let counter = 0;
      for (let key of testMap.keys()) {
        expect(key).toBeDefined();
        expect(key).not.toBeNull();
        counter++;
      }

      expect(counter).not.toBe(0);
      expect(testMap.size).not.toBe(0);
    }

  },

  entries: {

    "iterates all entries": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");
      map.set(4, "testing 4");
      map.set(5, "testing 5");

      let counter = 1;
      for (let [key, value] of map.entries()) {
        expect(key).toBe(counter);
        expect(value).toBe("testing " + counter);
        counter++;
      }

      expect(counter).toBe(6);
    },

    "iterates all entries skipping deleted": () => {
      // setup
      let expectedData = getIterationTestData();

      let testMap = new KeyMap();
      expectedData.forEach((expected) => {
        testMap.set(expected.key, expected.value);
      });

      // remove from expected
      expectedData.splice(2, 1);
      expectedData.pop();

      // remove from test map
      testMap.delete(2);
      testMap.delete(4);

      let counter = 0;

      for (let [key, value] of testMap.entries()) {
        expect(key).toBe(expectedData[counter].key);
        expect(value).toBe(expectedData[counter].value);
        counter++;
      }

      expect(counter).toBe(3);
    }

  },

  values: {

    "iterates all values": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");
      map.set(4, "testing 4");
      map.set(5, "testing 5");

      let counter = 1;
      for (let value of map.values()) {
        expect(value).toBe("testing " + counter);
        counter++;
      }

      expect(counter).toBe(6);
    },

    "iterates all entries skipping deleted": () => {
      // setup
      let expectedData = getIterationTestData();

      let testMap = new KeyMap();
      expectedData.forEach((expected) => {
        testMap.set(expected.key, expected.value);
      });

      // remove from expected
      expectedData.splice(2, 1);
      expectedData.pop();

      // remove from test map
      testMap.delete(2);
      testMap.delete(4);

      let counter = 0;
      for (let value of testMap.values()) {
        expect(value).toBe(expectedData[counter].value);
        counter++;
      }

      expect(counter).toBe(3);
    }

  },

  size: {

    "returns the count of values": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");
      map.set(4, "testing 4");
      map.set(5, "testing 5");
      expect(map.size).toBe(5);
    },

    "returns the count of values after deletion": () => {
      // setup
      let map = new KeyMap();
      map.set(1, "testing 1");
      map.set(2, "testing 2");
      map.set(3, "testing 3");
      map.set(4, "testing 4");
      map.set(5, "testing 5");

      // remove from test map
      map.delete(2);
      map.delete(4);

      expect(map.size).toBe(3);
    }

  },

  toTable: {

    "returns a formatted table": () => {
      let map = new KeyMap()
      map.set(1, { x: 1, y: 2, z: 3 })
      map.set(2, { x: 4, y: 5, z: 6 })
      map.set(3, { x: 7, y: 8, z: 9 })
      let table = map.toTable()

      expect(table.length).toBe(3)

      table.forEach((row, index) => {
        expect(row["entity.key"]).toBe(index + 1)
        expect(row["entity.type"]).toBe("Object")
        expect(row.x).toBeDefined()
        expect(row.y).toBeDefined()
        expect(row.z).toBeDefined()
      })

    },

    "returns all table without keys": () => {
      let map = new KeyMap()
      map.set(1, { x: 1, y: 2, z: 3 })
      map.set(2, { x: 4, y: 5, z: 6 })
      map.set(3, { x: 7, y: 8, z: 9 })
      let table = map.toTable(true)

      expect(table.length).toBe(3)

      table.forEach(row => {
        expect(row["entity.key"]).toBeUndefined()
        expect(row["entity.type"]).toBe("Object")
        expect(row.x).toBeDefined()
        expect(row.y).toBeDefined()
        expect(row.z).toBeDefined()
      })
    }

  }

}